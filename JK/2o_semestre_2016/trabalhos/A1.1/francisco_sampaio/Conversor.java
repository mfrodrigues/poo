/*
*O usuário informará o valor em Real.
*O programa exibe a conversão do valor em Dólar. 
*E logo após abre um link para informações sobre as cotações atuais.
 */
package conversor;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Scanner;

/**
 * @author Francisco Sampaio Moreira
 */

public class Conversor {

    public static void main(String[] args) {
        Scanner input = new Scanner (System.in);
            float real,dolar,valor; //Declarando as variaveis;
            System.out.println("Digite o valor em Real:");//Imprimindo a mensagem
            real = input.nextFloat(); //Variavel que recebe o valor em Real;
            dolar = (float) 3.2296; //Atribuindo valor à cotação;
            valor = dolar * real; //Atribuindo valor à variável valor
            System.out.println("Valor em Dólar:"+valor); //Imprimindo o valor em dólar;
            System.out.println("OBTENHA MAIS INFORMAÇÕES SOBRE COTAÇÔES NO SITE!"); //Imprimindo a mensagem;
        
        
        Desktop desk = java.awt.Desktop.getDesktop();   
            try {  //Tratamento de exceção;
                desk.browse(new java.net.URI("http://economia.uol.com.br/cotacoes/"));  
                    } catch (URISyntaxException | IOException e) {  
                    }
        
        
        
    }
    
}
